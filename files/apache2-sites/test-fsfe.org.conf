# This is needed for content negotiation (multiple languages)
<Directory /srv/www/test.fsfe.org/*>
    AllowOverride FileInfo Limit Indexes
    Options +MultiViews -Indexes
    Require all granted
</Directory>

# Don't serve dotfiles/dirs
<Files ~ "^\.">
    Require all denied
</Files>

<VirtualHost *:443>

    #
    # HTTP host config
    #

    ServerName test.fsfe.org
    ServerAlias test.fsfeurope.org
    ServerAdmin webmaster@fsfe.org


    DocumentRoot /srv/www/test.fsfe.org/global/
    
    ScriptAlias /cgi-bin/ /srv/www/test.fsfe.org/global/cgi-bin/
    <Directory "/srv/www/test.fsfe.org/global/cgi-bin/">
      AllowOverride None
      Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
      Require all granted
    </Directory>

    ErrorLog /var/log/apache2/test.fsfe.org-error.log
    CustomLog /var/log/apache2/test.fsfe.org-access.log combined

    #
    # TLS config
    #

    SSLEngine on
    SSLCertificateFile /etc/letsencrypt/live/fsfe.org/cert.pem
    SSLCertificateKeyFile /etc/letsencrypt/live/fsfe.org/privkey.pem
    SSLCertificateChainFile /etc/letsencrypt/live/fsfe.org/chain.pem

    SetEnvIf User-Agent ".*MSIE.*" nokeepalive ssl-unclean-shutdown

    SSLProtocol             all -SSLv2 -SSLv3 -TLSv1 -TLSv1.1
    SSLHonorCipherOrder     on
    SSLCipherSuite          TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384

    #
    # security headers
    # TODO: make as restrictive as possible
    #

    Header always set Strict-Transport-Security "max-age=15768000"

    #Prevent attack described on https://httpoxy.org/
    RequestHeader unset Proxy early

    RewriteEngine on

    # Make sure that permanent redirects are not cached indefinitely,
    # but re-validated every 10 minutes.
    Header append Cache-Control s-maxage=600 "expr=%{REQUEST_STATUS} == 301"
    Header append Cache-Control max-age=600 "expr=%{REQUEST_STATUS} == 301"


    # Beautiful error pages
    ErrorDocument 400 /error/400
    ErrorDocument 401 /error/401
    ErrorDocument 403 /error/403
    ErrorDocument 404 /error/404
    ErrorDocument 405 /error/405
    ErrorDocument 408 /error/408
    ErrorDocument 410 /error/410
    ErrorDocument 411 /error/411
    ErrorDocument 412 /error/412
    ErrorDocument 413 /error/413
    ErrorDocument 414 /error/414
    ErrorDocument 415 /error/415
    ErrorDocument 500 /error/500
    ErrorDocument 501 /error/501
    ErrorDocument 502 /error/502
    ErrorDocument 503 /error/503


    # Block nasty freebie sites
    # block iframes
    Header append X-FRAME-OPTIONS "SAMEORIGIN"
    # block based on certain referers
    RewriteCond %{HTTP_REFERER} .*telegra.ph/Besplatnye-vozdushnye-shary-i-naklejki-02-19 [NC,OR]
    RewriteCond %{HTTP_REFERER} .*zakupersi.com.* [NC,OR]
    RewriteCond %{HTTP_REFERER} .*pepper.pl.* [NC,OR]
    RewriteCond %{HTTP_REFERER} .*pl-pepper.digidip.net.* [NC,OR]
    RewriteCond %{HTTP_REFERER} .*dealdoktor.de.* [NC,OR]
    RewriteCond %{HTTP_REFERER} .*monetenfuchs.de.* [NC,OR]
    RewriteCond %{HTTP_REFERER} .*devswag.io.* [NC,OR]
    RewriteCond %{HTTP_REFERER} .*github.com/Joonsang1994/free-tshirts-stickers-and-swag-for-developers.* [NC,OR]
    RewriteCond %{HTTP_REFERER} .*devrant.com/rants/.* [NC]
    RewriteCond %{REQUEST_URI} ^/?promo.*$ [NC,OR]
    RewriteCond %{REQUEST_URI} ^/?contribute/spreadtheword[^-].*$ [NC]
    RewriteRule .* /contribute/spreadtheword-freebie.html [L,R]


    # Allow CORS for fsfe.org domains
    SetEnvIf Origin ^(https?://.+\.fsfe\.org(?::\d{1,5})?)$   CORS_ALLOW_ORIGIN=$1
    Header append Access-Control-Allow-Origin  %{CORS_ALLOW_ORIGIN}e   env=CORS_ALLOW_ORIGIN
    Header merge  Vary "Origin"


    # use robots_test.txt as robots.txt for test.fsfe.org
    Alias /robots.txt /srv/www/test.fsfe.org/global/robots_test.txt

</VirtualHost>

# rewrite all HTTP requests to HTTPS

<VirtualHost *:80>

    ServerName test.fsfe.org
    ServerAlias test.fsfeurope.org
    ServerAdmin webmaster@fsfe.org

    RewriteEngine On
    RewriteRule (.*) https://%{SERVER_NAME}$1 [R,L]

</VirtualHost>

SSLHonorCipherOrder     on
SSLCompression          off
SSLSessionTickets       off
SSLUseStapling          on
SSLStaplingResponderTimeout 5
SSLStaplingReturnResponderErrors off
SSLStaplingCache        shmcb:/var/run/ocsp(128000)

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
