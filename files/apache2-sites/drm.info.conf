# This is needed for content negotiation (multiple languages)
<Directory /srv/www/html/*>
    AllowOverride FileInfo Limit Indexes AuthConfig
    Options +MultiViews -Indexes
    Require all granted
</Directory>

<VirtualHost *:443>

    #
    # HTTP host config
    #

    ServerName drm.info
    ServerAlias www.drm.info
    ServerAdmin webmaster@fsfe.org

    DocumentRoot /srv/www/fsfe.org/global/drm.info/
    
    <Directory /srv/www/fsfe.org/global/drm.info/>
      Options +MultiViews +FollowSymlinks
      Require all granted
    </Directory>

    ErrorLog /var/log/apache2/drm.info-error.log
    CustomLog /var/log/apache2/drm.info-access.log combined

    #
    # TLS config
    #

    SSLEngine on
    SSLCertificateFile /etc/letsencrypt/live/drm.info/cert.pem
    SSLCertificateKeyFile /etc/letsencrypt/live/drm.info/privkey.pem
    SSLCertificateChainFile /etc/letsencrypt/live/drm.info/chain.pem

    # TODO: obsolete, needs replacement?
    # SSLCACertificateFile /etc/ssl/certs/StartSSL_root.pem
    SetEnvIf User-Agent ".*MSIE.*" nokeepalive ssl-unclean-shutdown

    SSLProtocol             all -SSLv2 -SSLv3 -TLSv1 -TLSv1.1
    SSLHonorCipherOrder     on
    SSLCipherSuite          TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384

    #
    # security headers
    # TODO: make as restrictive as possible
    #

    Header always set Strict-Transport-Security "max-age=15768000"

    #Prevent attack described on https://httpoxy.org/
    RequestHeader unset Proxy early

    RewriteEngine on
    RewriteRule ^/drm.info/(.*) https://drm.info/$1

    # Rewrite into canonical hostname drm.info
    RewriteCond %{HTTP_HOST} !(^drm\.info) [NC]
    RewriteCond %{HTTP_HOST} !^$
    RewriteRule ^/(.*) %{REQUEST_SCHEME}://drm.info/$1 [L,R=permanent]

</VirtualHost>

# rewrite all HTTP requests to HTTPS

<VirtualHost *:80>

    ServerName drm.info
    ServerAlias www.drm.info
    ServerAdmin webmaster@fsfe.org

    RewriteEngine On
    RewriteRule (.*) https://%{SERVER_NAME}$1 [R,L]

</VirtualHost>

SSLHonorCipherOrder     on
SSLCompression          off
SSLSessionTickets       off
SSLUseStapling          on
SSLStaplingResponderTimeout 5
SSLStaplingReturnResponderErrors off
SSLStaplingCache        shmcb:/var/run/ocsp(128000)

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
