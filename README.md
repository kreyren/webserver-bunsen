# FSFE Webserver

## Usage

Clone this repo:

```
git clone --recurse-submodules git@git.fsfe.org:fsfe-system-hackers/webserver-bunsen.git
```

Update the inventory submodule to reflect the newest changes to the list of our hosts and the groups that they are in:

```
git submodule update --remote inventory
```

Deploy/update all webservers:

```
ansible-playbook -i inventory/hosts playbook.yaml
```

Deploy/update only one webserver:

```
ansible-playbook -i inventory/hosts -l bunsen.fsfeurope.org playbook.yaml
```

## Notes

The file `files/php/99-custom.ini` is not deployed manually yet.
